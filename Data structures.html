<!DOCTYPE html>
<html>
	<head>
		<title>Data Structures</title>
		<link rel="stylesheet" href="specifications.css">
		<script src="specifications.js"></script>
		<meta name="viewport" content="width=device-width">
	</head>
	
	<body>
		<h1>Data Structures</h1>
		
		<section>
			<p>This document defines and describes the structure of different objects within SmartGuest. In some ways it describes what tables and columns should be in the database, but the actual implementation <em>does not</em> have to identically match these definitions as long as the outcome is the same. It is just a conceptual model. For example, a <span class="pre">Start date</span> attribute is not required to be in any particular format so long as it can represent the date an Activity begins.</p>
			
			<section id="attribute-bubbling">
				<h3>Attribute "bubbling"</h3>
				<figure>
					<img src="img/Attribute bubbling.png">
				</figure>
				
				<p>Many attributes of Activities, Places, and Categories will not have a value for their own use. For example, there is no phone number to call for a particular hiking activity, but the park which hosts the hike does have a phone number. Instead of leaving the phone number for the hike empty and having interested people unable to call for more information, we can use the number for the park on the hike's details page. The same goes for many other attributes: Images, phone numbers, social URLs, website URLs, and email addresses can all use the value from associated Places and Categories.</p>
				
				<p>For an <strong>Activity</strong>, the process is:</p>
				<ol>
					<li>Do I have the attribute?</li>
					<li>If I have a Place, does it have the attribute?</li>
					<li>If I have a Category, does it have the attribute?</li>
					<li>If my Category has a Place, does it have the attribute?</li>
					<li>If there is a default Place, does it have the attribute?</li>
				</ol>
				
				<p><strong>Categories</strong> only bubble their image, and their process is a bit different:</p>
				<ol>
					<li>Do I have the attribute?</li>
					<li>If I have a Place, does it have the attribute?</li>
					<li>If there is a default Place, does it have the attribute?</li>
				</ol>
				
				<p>For a <strong>Place</strong>, the process is much shorter:</p>
				<ol>
					<li>Do I have the attribute?</li>
					<li>If there is a default Place, does it have the attribute?</li>
				</ol>
				
				<p>Note that <strong>this process will overlap itself</strong>: If an Activity tries to find a value, it will look at its Place, which should technically follow its own process to try and find a value from the default Place. We want to use the values from the Category or Category's Place first, though, so the bubbling process must make sure to look at the true value of objects instead of relying on a bubbled value.</p>
			</section>
		</section>
		
		<hr>
		
		<section id="users">
			<h2>Users</h2>
			<p>User accounts define the credentials which are allowed access to the CMS. For the initial version there will be no user roles, all users have the same level of permissions.</p>
			
			<p>User accounts consist of only two attributes:</p>
			<ul>
				<li data-updated="2014-12-31"><strong>Username</strong>: Any unique string. Required.</li>
				<li data-updated="2014-12-31"><strong>Password</strong>: A cryptographically-secure hash (such as SHA-1) of the user's password. Required.</li>
			</ul>
			
			<p data-updated="2014-12-31">A default account with the username <span class="literal">"admin"</span> and the password <span class="literal">"12345"</span> will be generated when the CMS is created.</p>
		</section>
		
		<hr>
		
		<section id="activities">
			<h2>Activities</h2>
			<p>Activities are one of the main units of interaction in SmartGuest. They are used in the calendar, the directory, and several other places throughout the mobile app to show guests what is available to them. Activities themselves consist of these attributes:</p>
			<ul>
				<li><strong>Name</strong>: A string. Required.</li>
				<li><strong>Description</strong>: A multi-line string that can accept HTML.</li>
				<li><strong>Image</strong>: A string containing a partial URL (relative to the API) to use for this activity's image.</li>
				<li><strong>Category</strong>: A reference to the Category this Activity is a part of. Not all Activities have a Category.</li>
				<li><strong>Place</strong>: A reference to the Place where this Activity occurs. Not all Activities have a Place.</li>
				<li><strong>Phone number</strong>: A string matching the <a href="http://tools.ietf.org/html/rfc3966#section-3">tel URI syntax</a>.</li>
				<li><strong>Website URL</strong>: A string matching the <a href="https://tools.ietf.org/html/rfc3986">generic URI syntax</a>.</li>
				<li><strong>Email address</strong>: A string.</li>
				<li><strong>Series</strong>: References to the Series objects of this Activity.</li>
			</ul>
			
			<p>Activities' image, phone number, website URL, and email address are all subject to <a href="#attribute-bubbling">bubbling</a>.</p>
		</section>
		
		<hr>
		
		<section id="series-and-occurrences">
			<h2>Series and Occurrence objects</h2>
			<p>Activities have two types of timing-related objects associated with them: <strong>Series</strong> objects store the <em>abstract</em> information about a series of repetition (like whether it occurs daily or weekly) and the <em>specific</em> information that will be copied to each occurrence of the activity (like the start and end time). <strong>Occurrence</strong> objects store the individual instances an activity occurs on for a faster lookup when the system needs to know what is happening on a given day.</p>
			
			<p>For example, a New Year's Day event will have a Series which contains attributes for "every year on January 1st, from 10pm to 2am"; and Occurrences on January 1st, 2014; January 1st, 2015; January 1st, 2016...</p>
			
			<p>All Activities have a Series, even one-time events. They just have only one Occurrence associated with the Series.</p>
			
			<section id="series">
				<h3>Series</h3>
				<p>Series store most of the details for when Activities happen:</p>
				<ul>
					<li><strong>Activity</strong>: A reference to the Activity this Series object is associated with.</li>
					<li><strong>Start date</strong>: Which date the Occurrences should begin.</li>
					<li><strong>End date</strong>: Which date the last possible Occurrence should start.</li>
					<li><strong>Start time</strong>: Which time an Occurrence should begin on each valid date.</li>
					<li><strong>End time</strong>: Which time an Occurrence should end on each valid date.</li>
					<li><strong>Repetition</strong>: The type of repetition this Series should follow. Can be:</li>
					<ul>
						<li><span class="literal">"Once"</span></li>
						<li><span class="literal">"Day"</span></li>
						<li><span class="literal">"Week"</span></li>
						<li><span class="literal">"Month by number"</span></li>
						<li><span class="literal">"Month by day"</span></li>
						<li><span class="literal">"Month by position"</span></li>
						<li><span class="literal">"Year"</span></li>
					</ul>
					<li><strong>Repetition frequency</strong>: How many days, weeks, months, or years the repetition should skip between occurrences.</li>
					<li><strong>Week days</strong>: When the repetition is <span class="literal">"Week"</span>, this is a list of days of the week the event should occur. Unused in other repetitions.</li>
					<li><strong>Day number</strong>: When the repetition is <span class="literal">"Month by number"</span>, this is which day of the month the event should occur. Unused in other repetitions. The value can be any number from 1 to 31, provided the month has that many days.</li>
					<li data-updated="2014-12-31"><strong>Week number</strong>: When the repetition is <span class="literal">"Month by day"</span>, this is which week the event should occur. Unused in other repetitions. The value can be:
						<ul>
							<li><span class="literal">"First"</span></li>
							<li><span class="literal">"Second"</span></li>
							<li><span class="literal">"Third"</span></li>
							<li><span class="literal">"Fourth"</span></li>
							<li><span class="literal">"Last"</span></li>
						</ul>
					</li>
					<li data-updated="2014-12-31"><strong>Week day</strong>: When the repetition is <span class="literal">"Month by day"</span>, this is which day of the week the event should occur. Unused in other repetitions.</li>
					<li><strong>Position</strong>: When the repetition is <span class="literal">"Month by position"</span>, this is which position the event should occur. Unused in other repetitions. The value can be <span class="literal">"First"</span> or <span class="literal">"Last"</span>.</li>
				</ul>
				
				<p>When a Series is created, the system creates Occurrences for each iteration of the settings that were saved. These Occurrences reference the Series which created them, so when that Series is edited, all referenced Occurrences can be deleted and the "building" process can be repeated.</p>
			</section>
			
			<section id="occurrences">
				<h3>Occurrences</h3>
				<p>Compared to Series, Occurrences store very little information. They are created for each specific date that fulfills the Series' repetition and reference almost everything from the Series itself, so their only attributes are:</p>
				<ul>
					<li><strong>Series</strong>: A reference to the Series of this Occurrence.</li>
					<li><strong>Date</strong>: The date of this specific Occurrence.</li>
				</ul>
			</section>
			
			<p>Here are some examples of different Activities, and how they could be organized into Series and Occurrence objects:</p>
			
			<aside>
				<p>The pizza bar is open from 5pm to 9pm every Monday, Wednesday, and Friday. This activity's start date is February 10th, 2013 and end date is February 10th, 2014.</p>
				<pre>activity(id=1, name="Pizza bar", series=[1], ...)</pre>
				<pre> </pre>
				<pre>series(id=1, activity=1, startDate=2013/02/10, endDate=2014/02/10, startTime=1700, endTime=2100)</pre>
				<pre> </pre>
				<pre>occurrence(series=1, date=2013/02/11)</pre>
				<pre>occurrence(series=1, date=2013/02/13)</pre>
				<pre>occurrence(series=1, date=2013/02/15)</pre>
				<pre>occurrence(series=1, date=2013/02/18)</pre>
				<pre>...</pre>
				<pre>occurrence(series=1, date=2014/02/05)</pre>
				<pre>occurrence(series=1, date=2014/02/07)</pre>
				<pre>occurrence(series=1, date=2014/02/10)</pre>
			</aside>
			
			<aside>
				<p>There is a yoga class every Tuesday from 6am to 7am in the month of March. Except it's cancelled on March 12th.</p>
				<pre>activity(id=2, name="Yoga class", series=[2, 3], ...)</pre>
				<pre> </pre>
				<pre>series(id=2, activity=2, startDate=2013/03/01, endDate=2013/03/11, startTime=0600, endTime=0700)</pre>
				<pre>occurrence(series=2, date=2013/03/05)</pre>
				<pre> </pre>
				<pre>series(id=3, activity=2, startDate=2013/03/13, endDate=2013/03/31, startTime=0600, endTime=0700)</pre>
				<pre>occurrence(series=3, date=2013/03/19)</pre>
				<pre>occurrence(series=3, date=2013/03/26)</pre>
			</aside>
			
			<aside>
				<p>Dr. Foobar will be giving a lecture on relativistic propulsion from 3pm to 7pm on Wednesday, April 17th, 2013.</p>
				<pre>activity(id=3, name="Relativistic propulsion lecture", seriesIDs=[3], ...)</pre>
				<pre> </pre>
				<pre>series(id=3, activity=3, startDate=2013/04/17, endDate=2013/04/17, startTime=1500, endTime=1900)</pre>
				<pre>occurrence(series=3, date=2013/04/17)</pre>
			</aside>
		</section>
		
		<hr>
		
		<section id="categories">
			<h2>Categories</h2>
			<p>Categories are an optional container for Activities, to break them down into sensible groups and avoid having Activities with very long names like <span class="literal">"Stonebrook Golf - Front 9 - Youth"</span>. Categories store many of the same attributes as Activities, but don't show their attributes the same way in the mobile app. They're mostly used for bubbling.</p>
			<ul>
				<li><strong>Name</strong>: A string. Required.</li>
				<li><strong>Description</strong>: A multi-line string that can accept HTML.</li>
				<li><strong>Image</strong>: A string containing a partial URL (relative to the API) to use for this category's image.</li>
				<li><strong>Place</strong>: A reference to the Place where all the Activities in this Category occur. Not all Categories have a Place.</li>
				<li><strong>Phone number</strong>: A string matching the <a href="http://tools.ietf.org/html/rfc3966#section-3">tel URI syntax</a>.</li>
				<li><strong>Website URL</strong>: A string matching the <a href="https://tools.ietf.org/html/rfc3986">generic URI syntax</a>.</li>
				<li><strong>Email address</strong>: A string.</li>
				<li><strong>Activities</strong>: References to the Activities in this Category.</li>
			</ul>
			
			<p data-updated="2014-12-31">A Category's image is the only attribute that <a href="#attribute-bubbling">bubbles</a>.</p>
		</section>
		
		<hr>
		
		<section id="places">
			<h2>Places</h2>
			<p>Places are the locations within the site and local businesses in the area that an Activity can be attached to. Their job is to provide an address and contact details, so the guest can physically find the event or contact the host of an event.</p>
			
			<p>Places have the following attributes:</p>
			<ul data-updated="2014-12-31">
				<li><strong>Name</strong>: A string. Required.</li>
				<li><strong>Description</strong>: A multi-line string that can accept HTML.</li>
				<li><strong>Image</strong>: A string containing a partial URL (relative to the API) to use for this place's image.</li>
				<li><strong>Location</strong>: A multi-line string.</li>
				<li><strong>Phone number</strong>: A string matching the <a href="http://tools.ietf.org/html/rfc3966#section-3">tel URI syntax</a>.</li>
				<li><strong>Email address</strong>: A string.</li>
				<li><strong>Facebook URL</strong>: A string matching the <a href="https://tools.ietf.org/html/rfc3986">generic URI syntax</a>.</li>
				<li><strong>Twitter URL</strong>: A string matching the <a href="https://tools.ietf.org/html/rfc3986">generic URI syntax</a>.</li>
				<li><strong>Foursquare URL</strong>: A string matching the <a href="https://tools.ietf.org/html/rfc3986">generic URI syntax</a>.</li>
				<li><strong>Website URL</strong>: A string matching the <a href="https://tools.ietf.org/html/rfc3986">generic URI syntax</a>.</li>
				<li><strong>On-site</strong>: A boolean value that determines whether this Place should be considered on-site or off-site.</li>
				<li><strong>Primary</strong>: A boolean value that identifies the one Place all others use to get fallback values from. Only one place can be marked primary at a time.</li>
			</ul>
			
			<p>Places' image, location, phone number, email address, social network URLs, and website URL are all subject to bubbling.</p>
			
			<p>Keep in mind that Places don't have the same bubbling hierarchy as Activities. If a Place does not have a given attribute, the only object that is checked for a fallback value is the primary Place.</p>
		</section>
		
		<hr>
		
		<section id="main-menu-items">
			<h2>Main menu items</h2>
			<p>The main menu items appear in the mobile app's "Directory" screen and come in two flavors: <strong>Default menu items</strong> direct the mobile app to certain pre-defined collections of objects, and <strong>custom menu items</strong> are defined by the client to include the specific Categories, Activities, and Places they want to feature.</p>
			
			<section id="default-menu-items">
				<h3>Default menu items</h3>
				<p>The default menu items can be renamed and turned on and off; but what they contain cannot be controlled. Each menu item has the following attributes:</p>
				<ul data-updated="2014-12-31">
					<li><strong>Name</strong>: A string. Required.</li>
					<li><strong>Active</strong>: A boolean value determining the visibility of the item.</li>
					<li><strong>Description</strong>: Fixed help text describing what this menu item will contain. This cannot be changed.</li>
					<li><strong>Sort ID</strong>: The order in which this item appears. Must be a unique number across the default <em>and</em> custom items.</li>
				</ul>
				
				<p>There are a number of default menu items that exist when the system is created:</p>
				<table>
					<tr>
						<th>Name</th>
						<th>Description</th>
					</tr>
					<tr>
						<td><span class="literal">"On-site activities"</span></td>
						<td><span class="literal">"Shows all activities associated with an on-site place."</span></td>
					</tr>
					<tr>
						<td><span class="literal">"Off-site activities"</span></td>
						<td><span class="literal">"Shows all activities associated with an off-site place."</span></td>
					</tr>
					<tr>
						<td><span class="literal">"A-Z directory"</span></td>
						<td><span class="literal">"Shows the full list of categories, activities, and places."</span></td>
					</tr>
				</table>
			</section>
			
			<section id="custom-menu-items">
				<h3>Custom menu items</h3>
				<p>Custom items are very similar to the default items, but they can contain a custom set of items. They have these attributes:</p>
				<ul>
					<li><strong>Name</strong>: A string. Required.</li>
					<li><strong>Active</strong>: A boolean value determining the visibility of the item.</li>
					<li><strong>Children</strong>: References to Categories, Activities, and Places contained within this menu.</li>
					<li><strong>Image</strong>: A string containing the image's URL relative to the API.</li>
					<li data-updated="2014-12-31"><strong>Description</strong>: A string containing text to use in the body when showing a link to this menu.</li>
					<li><strong>Sort ID</strong>: The order in which this menu item appears. Must be a unique number across the default <em>and</em> custom items.</li>
				</ul>
				
				<p>No custom menu items exist when the system is created.</p>
			</section>
		</section>
		
		<hr>
		
		<section id="mobile-app-configuration" data-updated="2014-12-31">
			<h2>Mobile app configuration</h2>
			<p>There is only one Configuration, instead of it being a generic "class" of object. This is the only data structure different from other objects. The attributes contained within the configuration are:</p>
			<ul>
				<li><strong>Location name</strong>: A string.</li>
				<li><strong>Logo</strong>: A string containing the logo's URL relative to the API.</li>
				<li><strong>Background</strong>: A string containing the background's URL relative to the API.</li>
				<li data-updated="2014-12-29"><strong>Splash</strong>: A string containing the splash page image's URL relative to the API.</li>
				<li><strong>Primary</strong>: A string containing a CSS color value.</li>
				<li><strong>Accent</strong>: A string containing a CSS color value.</li>
				<li><strong>Theme</strong>: A string containing the value <span class="literal">"Light"</span> or <span class="literal">"Dark"</span>.</li>
			</ul>
		</section>
		
		<hr>
		
		<section id="daily-welcome-messages">
			<h2>Daily welcome messages</h2>
			<p>A hotel usually posts a printed welcome message from the General Manager which describes some of the services and amenities available to the guests. The Daily Welcome messages in SmartGuest are designed to replace this while offering the ability to be changed on a daily basis. A message consists of these attributes:</p>
			<ul>
				<li data-updated="2014-12-31"><strong>Name</strong>: A string. Required.</li>
				<li><strong>Message</strong>: A string which can accept HTML. Required.</li>
				<li><strong>Days</strong>: A list of days of the week this message is shown on. Only one message can have a particular day in its list.</li>
				<li><strong>Default</strong>: A boolean value determining whether this message should be shown when there is no other message for a particular day. Only one message can be marked as the default.</li>
			</ul>
			
			<p>A message cannot be set to show up on a set of days <em>and</em> be the default. It can only have a set of days, be the default, or have neither.</p>
			
			<p>Since there can only ever be one message on a particular day, when a message is created or updated, all of the days the message has checked are removed from all other days. The same goes for the "default" message. This allows the user to "overwrite" a given day's message (or the default message) by selecting it on the desired message.</p>
		</section>
		
		<hr>
		
		<section id="special-offers">
			<h2>Special offers</h2>
			<p>Offers appear on the app's "Home" screen, and can rotate each day, or be set up as "fallbacks" in case there aren't enough offers to show. Offers are made up of the following attributes:</p>
			<ul>
				<li><strong>Title</strong>: A string. Required.</li>
				<li><strong>Snippet</strong>: A string.</li>
				<li><strong>Body</strong>: A string which can accept HTML.</li>
				<li><strong>Days</strong>: A list of days this offer should be shown on. An offer cannot have days and be a fallback.</li>
				<li data-updated="2014-12-31"><strong>Fallback</strong>: A boolean value. These offers are used as "fallback" items to be shown when there are not enough offers applicable to the current day.</li>
				<li data-updated="2014-12-31"><strong>Place</strong>: A reference to the Place associated with this offer.</li>
			</ul>
			
			<p>An offer cannot be set to show up on a set of days <em>and</em> be a fallback. It can only have a set of days, be a fallback, or have neither.</p>
		</section>
		
		<hr>
		
		<section id="push-notifications">
			<h2>Push notifications</h2>
			<figure>
				<img src="img/Notification flow.png">
			</figure>
			
			<p>Users have the ability to send push notifications directly to guests, as well as set them up on a recurring schedule. This feature is primarily intended to notify guests of activities which have been cancelled, announcing open slots for activities, and emergency messages, but is flexible enough to be appropriated for any number of other uses.</p>
			
			<section id="immediate-notifications">
				<h3>Immediate notifications</h3>
				<p>An Immediate notification object consists of these attributes:</p>
				<ul>
					<li><strong>Heading</strong>: A string. Required.</li>
					<li><strong>Message</strong>: String which can accept HTML. Required.</li>
					<li data-updated="2014-12-31"><strong>App ID</strong>: The app ID of the intended recipient for this Notification. Notifications with an app ID only appear when including that ID in the query to the <a href="API specification.html#notifications-endpoint">Notifications endpoint</a>.</li>
					<li><strong>Datetime</strong>: The datetime at which the Immediate notification was created.</li>
					<li><strong>Expiration</strong>: The datetime at which the Immediate notification is no longer valid.</li>
					<li><strong>Created from</strong>: If this notification is an occurrence of a Recurring notification, this attribute stores a reference to the Recurring notification. (The figure calls this a "Parent".)</li>
				</ul>
			</section>
			
			<section id="recurring-notifications">
				<h3>Recurring notifications</h3>
				<p>Like Series and Occurrence objects, Recurring notifications are used as the template for their individual occurrences; Immediate notifications, in this case. They contain the following attributes:</p>
				<ul>
					<li><strong>Heading</strong>: A string. Required.</li>
					<li><strong>Message</strong>: A string which can accept HTML. Required.</li>
					<li><strong>Days</strong>: A list of days on which Immediate notifications should be created from this template.</li>
					<li><strong>Start date</strong>: The first date on which Immediate notifications should be created.</li>
					<li><strong>End date</strong>: The last date on which Immediate notifications should be created.</li>
					<li><strong>Time</strong>: The time each Immediate notification is to be posted.</li>
					<li><strong>Expiration</strong>: The time each Immediate notification should expire.</li>
				</ul>
				
				<p>Recurring notifications are not capable of storing an app ID.</p>
			</section>
		</section>
		
		<hr>
		
		<section id="channels">
			<h2>Channels</h2>
			<p>The TV channels are available on the <a href="App specification.html#channels-page">"Channels" page</a>, and store very little data, since they are so fundamentally simple:</p>
			<ul>
				<li><strong>Number</strong>: A non-zero natural number (1, 2, 3, 42, 9000... but not 0). Required.</li>
				<li><strong>Name</strong>: A string. Required.</li>
			</ul>
			
			<p>A channel's number and name <em>are not required to be unique</em>.</p>
		</section>
		
		<hr>
		
		<section id="requests">
			<h2>Requests</h2>
			<p>Guest requests are the objects submitted by guests or staff for hotel staff to accomplish. A Request is actually divided into three pieces: A <span class="pre">Request Type</span>, the <span class="pre">Request</span> itself, and <span class="pre">Request Actions</span> that describe the Request's history. The system is almost identical to a typical support ticket system, stripped down to its core functionality for our clients' needs.</p>
			
			<p>Guests and employees can both create Requests: Guests use the "Requests" page in the app to look through the possible Types, find the one that applies to them, enter some details, and submit it. Employees create Requests by using the "New request" page in the CMS to handle guests who are speaking with them face-to-face or call in a request without using the app.</p>
			
			<p>The typical flow of a Request is:</p>
			<ul>
				<li>The guest submits a request through the app, or the front desk creates it.</li>
				<li>Whoever is responsible for that type of request, the "point of contact", sees the new Request in the list and dispatches it to whoever will do the actual work.</li>
				<li>The point of contact changes the status to <span class="literal">"Accepted"</span>.</li>
				<li>As work progresses, the point of contact updates the status of the Request.</li>
				<li>Once the work is complete, the point of contact changes the status to <span class="literal">"Completed"</span>.</li>
				<li>The guest has the opportunity to dismiss or re-open the Request.</li>
			</ul>
			
			<section id="request-types">
				<h3>Request types</h3>
				<p>Every property has a different set of requests that they typically receive from guests. A hotel on the beach may get more requests for amenities (like towels), a casino may get lots of requests for champagne delivered to the room, and an older property may get more maintenance requests than anything else.</p>
				
				<p>To remain flexible for all kinds of properties, employees can create request Types, which contain the following attributes:</p>
				<ul>
					<li><strong>Name</strong>: A string. Required.</li>
					<li><strong>Description title</strong>: A string which sets the label for the description section. A maintenance request may want to ask "What needs fixing?", while a housekeeping request may use "How can we help?", and a room service request would ask "What can we get for you?".</li>
					<li>Titles for each of the three fixed status options: <strong>New</strong>, <strong>Accepted</strong>, and <strong>Completed</strong>. This allows you to customize the words used for each status under a particular Type, since a wake up call request may make more sense to say "Ready to send" instead of "Accepted".</li>
				</ul>
				
				<p>The request Types show up on the app's <a href="App specifications.html#requests-page">"Requests" page</a>, and while there is no artificial limit on the number of Types that can be created, fewer Types makes it easier for the guest to scan for the appropriate one.</p>
				
				<p>In addition to the custom Types a property creates, there is an <strong>"Other" Type</strong> for requests that don't fit into any of the custom Types. All of its attributes can be changed just like any other request Type, including the name, but it can't be deleted.</p>
			</section>
			
			<section id="request-objects">
				<h3>Requests</h3>
				<p>The main object is the Request. Each Request has the following attributes:</p>
				<ul>
					<li><strong>App ID</strong>: The unique ID of the application which submitted the Request, so a Request can be tied to a user. If the Request was created from the CMS, its app ID will be <span class="literal">"CMS"</span>.</li>
					<li><strong>Type</strong>: The <a href="#request-type">Request Type</a> of this particular Request.</li>
					<li><strong>Room</strong>: A string. This contains the room number of the guest needing attention. It must be capable of storing any printable character, as some clients use simple numbering schemes like "103", while others use names like "VIP suite".</li>
					<li><strong>Name</strong>: A string. This contains the name of the guest needing attention.</li>
					<li><strong>Description</strong>: A string. This contains the descriptive text entered by the guest or employee who created the Request.</li>
					<li><strong>Status</strong>: A reference to the current status of the Request.</li>
					<li><strong>Status message</strong>: A string containing the displayable version of the status. (See the <a href="#request-statuses">"What do the statuses mean?" section</a>.)</li>
					<li><strong>Last updated</strong>: The datetime this Request was last acted upon. This is determined by finding the most recent datetime of the Request Actions associated with this Request.</li>
				</ul>
			</section>
			
			<section id="request-statuses">
				<h3>What do the statuses mean?</h3>
				<p>Every request has the same six status options: <span class="literal">"New"</span>, <span class="literal">"Accepted"</span>, <span class="literal">"Complete"</span>, <span class="literal">"Dismissed"</span>, <span class="literal">"Cancelled"</span>, and <span class="literal">"Other"</span>. An employee can change the status of a Request to New, Accepted, Complete, Cancelled, or Other, but cannot change it to Dismissed. At first glance it may not be obvious why the feature is designed this way, so consider what each status option is for:</p>
				<ul>
					<li><strong>New</strong> requests have just recently been entered into the system, and have not yet been acted upon.</li>
					<li>A request is <strong>Accepted</strong> after an employee dispatches it for work. Changing the status to Accepted lets the guest know that someone has seen their request and is doing something about it.</li>
					<li>Requests are <strong>Completed</strong> when the work is finished and the request has been taken care of.</li>
					<li>Guests can mark a Request <strong>Cancelled</strong> when they decide they don't actually need it.</li>
					<li>A Request is <strong>Dismissed</strong> when a guest sees their Request is complete and confirms it. You could also call this status "The guest has confirmed that this request is actually finished and will not require any more work", but we use the much shorter "Dismissed" instead.</li>
					<li>The <strong>Other</strong> status allows employees to type in a custom message to update the guest on the progress of the request. For example, if maintenance has to go buy a new gasket to fix a leaky faucet, the status message could say "Waiting for parts" so the guest doesn't think the request has been forgotten.</li>
				</ul>
				
				<p>For the New, Accepted, and Completed requests, the actual titles can be configured through the request's Type. Dry cleaning requests might make more sense to say "Scheduled for pickup" instead of "Accepted".</p>
			</section>
			
			<section id="request-actions">
				<h3>Request Actions</h3>
				<p>Every time a Request is created, the status of a Request is changed, or the details of the Request are edited, a Request Action is generated. These objects are used to keep track of when certain things happen, so the API can respond to questions like "when was the last time this request was updated?". Request Actions have the following attributes:</p>
				<ul>
					<li><strong>Request</strong>: The Request this particular Request Action is associated with.</li>
					<li><strong>Datetime</strong>: What datetime this Request Action occurred.</li>
					<li><strong>Username</strong>: Which person took the action. If a guest is responsible for the Request Action this field contains an app ID, otherwise it contains the username of the CMS user.</li>
					<li><strong>Description</strong>: A string to explain what effect was taken.</li>
				</ul>
				
				<p>The description of a Request Action is determined by a simple set of rules:</p>
				<table>
					<tr>
						<th>Action</th>
						<th>Description</th>
					</tr>
					<tr>
						<td>A new Request is created</td>
						<td><span class="literal">"Created"</span></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"New"</span></td>
						<td><span class="literal">"Request re-opened"</span></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"Accepted"</span></td>
						<td><span class="literal">"Status changed to {title of accepted}"</span><small> (see below)</small></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"Completed"</span></td>
						<td><span class="literal">"Status changed to {title of completed}"</span><small> (see below)</small></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"Cancelled"</span></td>
						<td><span class="literal">"Request cancelled"</span></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"Dismissed"</span></td>
						<td><span class="literal">"Request dismissed"</span></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"Other"</span></td>
						<td><span class="literal">"Status changed to {status message}"</span></td>
					</tr>
					<tr>
						<td>The name or room is changed</td>
						<td><span class="literal">"{field} changed to {new value}"</span></td>
					</tr>
					<tr>
						<td>The Request's description is changed</td>
						<td><span class="literal">"Description changed"</span></td>
					</tr>
					<tr>
						<td>Multiple fields are changed</td>
						<td><span class="literal">"Request modified"</span></td>
					</tr>
				</table>
				
				<p>Changing the status to "Accepted" or "Completed" stores the <em>Type's title for the status</em> in the description, instead of literally the words <span class="literal">"accepted"</span> or <span class="literal">"completed"</span>. This keeps Request Types with customized titles consistent throughout the CMS. For example: Suppose we have a maintenance request, and the customized title of the Accepted status for maintenance requests reads "On the way". The description of the Request Action should read "Status to On the way", instead of "Status to Accepted".</p>
				
				<p>Note that this intentionally ignores what happens when a status title is changed <em>after</em> a Request Action is created. We don't go back and update the description of Request Actions to whatever the new status title might be, because it simply isn't a big enough problem to worry about.</p>
			</section>
			
			<section id="request-notifications" data-updated="2014-12-31">
				<h3>Request notifications</h3>
				<p>When a Request Action is created by anyone <em>other than the owner</em> of the Request, a new Notification is created and sent to the owner. The <strong>heading</strong> of the Notification depends on the action that was taken:</p>
				<table>
					<tr>
						<th>Action</th>
						<th>Heading</th>
					</tr>
					<tr>
						<td><strike>A new Request is created</strike></td>
						<td><i>(Can not create a Notification, since only the guest can create a Request associated with their app ID)</i></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"New"</span></td>
						<td><span class="literal">"Your {request type} request was re-opened"</span></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"Accepted"</span></td>
						<td><span class="literal">"Your {request type} request is now {title of accepted}"</span></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"Completed"</span></td>
						<td><span class="literal">"Your {request type} request is now {title of completed}"</span></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"Cancelled"</span></td>
						<td><span class="literal">"Your {request type} request was cancelled"</span></td>
					</tr>
					<tr>
						<td><strike>The status is changed to <span class="literal">"Dismissed"</span></strike></td>
						<td><i>(Can not create a Notification, since only the guest can dismiss a Request associated with their app ID)</i></td>
					</tr>
					<tr>
						<td>The status is changed to <span class="literal">"Other"</span></td>
						<td><span class="literal">"Your {request type} request is now {status message}"</span></td>
					</tr>
					<tr>
						<td>The name or room is changed</td>
						<td><span class="literal">"The {field} on your {request type} request was changed"</span></td>
					</tr>
					<tr>
						<td>The Request's description is changed</td>
						<td><span class="literal">"The description on your {request type} request was changed"</span></td>
					</tr>
					<tr>
						<td>Multiple fields are changed</td>
						<td><span class="literal">"Your {request type} request was modified"</span></td>
					</tr>
				</table>
				
				<p>The <strong>message</strong> of the Notification is always <span class="literal">"Your {request type} request was updated at {datetime}"</span>, and the "updated" datetime uses the format <span class="pre">h:mma</span> (for example: <span class="literal">"Your Housekeeping request was updated at 12:34pm"</span>).</p>
				
				<p>Finally, the <strong>app ID</strong> of the Notification is set to the app ID of the updated Request, so only the intended guest will be notified.</p>
			</section>
		</section>
		
		<hr>
		
		<section id="analytics" data-updated="2016-04-20">
			<h2>Analytics</h2>
			<p>The Analytics objects store information about actions taken by the app's users, to be used in analysis and reporting. They are created when the app makes API requests, so an Analytics object is essentially a record of what API request was made.</p>
			
			<p>An Analytics object has the following attributes:</p>
			
			<ul>
				<li><strong>Datetime</strong>: The date and time at which the request was made.</li>
				<li><strong>App ID</strong>: The unique <code>App ID</code> of the application making the request.</li>
				<li><strong>URL</strong>: The URL of the request.</li>
				<li><strong>Page ID</strong>: The ID of the URL's query string, if there was one.</li>
				<li><strong>Page Type</strong>: The API endpoint that was requested.</li>
			</ul>
		</section>
	</body>
</html>