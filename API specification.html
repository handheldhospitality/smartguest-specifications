<!DOCTYPE html>
<html>
	<head>
		<title>API and Core API Specifications</title>
		<link rel="stylesheet" href="specifications.css">
		<script src="specifications.js"></script>
		<meta name="viewport" content="width=device-width">
	</head>
	<body>
		<h1>API and Core API Specifications</h1>
		
		<section>
			<p>The API is the piece which glues the CMS and the mobile app together: Users operate the CMS to manage the database, and the API reads the database to provide useful output to the mobile app.</p>
			
			<p>Virtually all of the endpoints provided by the API are read-only, so they do not require any authentication or authorization to access.</p>
			
			<section id="requests-and-responses">
				<h3>Requests and responses</h3>
				<p>Unless otherwise specified, all <strong>requests</strong> are made as HTTP <span class="pre">GET</span> requests to the API server with parameters contained in the URI's query string. (The <a href="http://www.w3.org/TR/html5/forms.html#url-encoded-form-data">HTML5 spec</a> has the extremely technical details behind it, but basically it's <span class="pre">keyOne=valOne&amp;keyTwo=valTwo</span>.) If any of the data being requested has a <span class="pre">bestBy</span> date, it will be included in an <span class="pre">If-Modified-Since</span> header with the request.</p>
				
				<p><strong>Responses</strong> must use <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html#sec6.1.1">proper status codes</a> (especially <span class="pre">304 Not Modified</span> to minimize network traffic), must include a <span class="pre">Content-Type: text/json</span> header, and must encode the requested data into a JSON document in the body.</p>
				
				<p>All keys in JSON objects should be in <a href="http://en.wikipedia.org/wiki/CamelCase#Variations_and_synonyms">lower camel case</a>.</p>
			</section>
		</section>
		
		<hr>
		
		<section id="client-locations-endpoint">
			<h2>The "Client locations" endpoint</h2>
			<p>Before the mobile app can connect to the clients' API server, it has to know where to look. The client locations API provides the service to perform these lookups.</p>
			
			<p>This endpoint uses the same <a href="#requests-and-responses">request and response assumptions</a> the rest of the API uses.</p>
			
			<p>Each mobile application is configured to work with one client, no matter how many locations it has, so the application sends a <strong>request</strong> to an endpoint on HandHeld's server with this client ID in order to get a list of locations (and their connection details). It is formatted as:</p>
			<pre>GET http://accounts.smartguestapp.com/{client id}/locations</pre>
			
			<p>The <strong>response</strong> is an array of location objects with:</p>
			<ul>
				<li>A <span class="pre">name</span> attribute, containing the name to use when refering to the location.</li>
				<li>An <span class="pre">api</span> attribute, containing the complete URI of the API's starting point.</li>
				<li>A <span class="pre">status</span> attribute, which can either be <span class="literal">""available""</span> or <span class="literal">""unavailable""</span>.</li>
			</ul>
			
			<p>An example response:</p>
			<pre>[</pre>
			<pre>	{</pre>
			<pre>		name: "Green Lagoon Inn and Suites",</pre>
			<pre>		api: "http://greenlagoon.com/smartguest-api",</pre>
			<pre>		status: "available"</pre>
			<pre>	},</pre>
			<pre>	{</pre>
			<pre>		name: "Hotel HandHeld",</pre>
			<pre>		api: "https://hotelhandheld.example.com/mobile/api",</pre>
			<pre>		status: "unavailable"</pre>
			<pre>	},</pre>
			<pre>	{</pre>
			<pre>		...</pre>
			<pre>	},</pre>
			<pre>	{</pre>
			<pre>		name: "Camp Onomatopoeia",</pre>
			<pre>		api: "http://campono.com:8080",</pre>
			<pre>		status: "available"</pre>
			<pre>	}</pre>
			<pre>]</pre>
		</section>
		
		<hr>
		
		<section id="style-endpoint">
			<h2>"Style" endpoint</h2>
			<p>The app loads with a default set of styles, and replaces them with the location-specific style once the guest chooses a location. The <strong>request</strong> is formatted as:</p>
			<pre>GET {api}/style</pre>
			
			<p>The <strong>response</strong> is an object with attributes from the <a href="Data structures.html#mobile-app-configuration">Mobile app configuration</a>. An example response:</p>
			<pre>{</pre>
			<pre>	locationName: "Green Lagoon Inn",</pre>
			<pre>	logo: "http://greenlagoon.com/mobile_logo.png",</pre>
			<pre>	background: "images/App_background.jpg",</pre>
			<pre>	<span data-updated="2014-12-29">splash: "images/Splash_screen_picture.jpg",</span></pre>
			<pre>	primary: "rgb(255, 0, 0)",</pre>
			<pre>	accent: "rgb(128, 128, 128)",</pre>
			<pre>	theme: "Dark"</pre>
			<pre>}</pre>
		</section>
		
		<hr>
		
		<section id="special-offers-endpoint">
			<h2>"Special offers" endpoint</h2>
			<p>The <strong>request</strong> is formatted as:</p>
			<pre>GET {api}/offers</pre>
			
			<p data-updated="2014-12-31">The <strong>response</strong> contains an array of all <a href="Data structures.html#special-offers">special offer</a> objects applicable to the current day. If there are no offers for the current day, it returns "fallback" offers instead.</p>
			
			<p>The <span class="pre">days</span> and <span class="pre">fallback</span> attributes are not required in the <strong>response</strong>, otherwise an example could be:</p>
			<pre>[</pre>
			<pre>	{</pre>
			<pre>		id: 99,</pre>
			<pre>		title: "25% off lunch",</pre>
			<pre>		snippet: "Get 25% off your lunch when you eat at the clubhouse today!",</pre>
			<pre>		body: "...",</pre>
			<pre>		place: 18</pre>
			<pre>	},</pre>
			<pre>	{...},</pre>
			<pre>]</pre>
		</section>
		
		<hr>
		
		<section id="primary-place-endpoint">
			<h2>"Primary place" endpoint</h2>
			<p>This API query gets the primary Place's details to populate the <span class="literal">"Social links"</span> card on the app's "Home" page. The <strong>request</strong> is formatted as:</p>
			<pre>GET {api}/primaryPlace</pre>
			
			<p>The <strong>response</strong> contains the attributes of the primary <a href="Data structures.html#places">Place object</a>, such as:</p>
			<pre>{</pre>
			<pre>	id: 74,</pre>
			<pre>	name: "Hotel Handheld",</pre>
			<pre>	image: "http://hotelhospitality.example.com/lobby.jpg",</pre>
			<pre>	description: "...",</pre>
			<pre>	location: "...",</pre>
			<pre>	phone: "+118005556789",</pre>
			<pre>	email: "frontdesk@hotelhospitality.example.com",</pre>
			<pre>	url: "hotelhospitality.example.com",</pre>
			<pre>	onSite: true,</pre>
			<pre>	primary: true</pre>
			<pre>}</pre>
		</section>
		
		<hr>
		
		<section id="directory-endpoint">
			<h2>"<span data-updated="2014-12-31">Directory</span>" endpoint</h2>
			
			<p>The <strong>request</strong> is formatted as:</p>
			<pre>GET {api}/<span data-updated="2014-12-31">directory</span></pre>
			
			<p>The <strong>response</strong> is an array containing <a href="Data structures.html#main-menu-items">Main Menu Items</a>:</p>
			<pre>[</pre>
			<pre>	{</pre>
			<pre>		id: 1,</pre>
			<pre>		sortID: 1,</pre>
			<pre>		name: "Dining",</pre>
			<pre>		description: "...",</pre>
			<pre>		image: "images/Dining_menu.jpg"</pre>
			<pre>	},</pre>
			<pre>	{</pre>
			<pre>		id: 7,</pre>
			<pre>		sortID: 2,</pre>
			<pre>		name: "Outdoors",</pre>
			<pre>		description: "...",</pre>
			<pre>		image: "images/Picture for outdoors menu.jpg"</pre>
			<pre>	},</pre>
			<pre>	{</pre>
			<pre>		id: 4,</pre>
			<pre>		sortID: 3,</pre>
			<pre>		name: "Nightlife",</pre>
			<pre>		description: "...",</pre>
			<pre>		image: "images/Clubs and bars (1) - Newspaper (new version).png"</pre>
			<pre>	}</pre>
			<pre>]</pre>
		</section>
		
		<hr>
		
		<section id="menu-content-endpoint" data-updated="2014-12-31">
			<h2>"Menu contents" endpoint</h2>
			<p>This endpoint is used to discover the list of items inside of a particular menu. The <strong>request</strong> is formatted as:</p>
			<pre>GET {api}/menu?id={id}</pre>
			
			<p>The <strong>response</strong> is an array of <a href="Data structures.html#activities">Activity</a>, <a href="Data structures.html#places">Place</a>, and <a href="Data structures.html#categories">Category</a> objects containing the details necessary for display and navigation:</p>
			<pre>[</pre>
			<pre>	{</pre>
			<pre>		id: 4296,</pre>
			<pre>		name: "Arthouse",</pre>
			<pre>		type: "Place",</pre>
			<pre>		image: "images/Arthouse_1x1.png",</pre>
			<pre>		description: "..."</pre>
			<pre>	},</pre>
			<pre>	{</pre>
			<pre>		id: 10,</pre>
			<pre>		name: "Breakfast Buffet at The Rotunda",</pre>
			<pre>		type: "Activity",</pre>
			<pre>		image: "images/Breakfast buffet.webp",</pre>
			<pre>		description: "..."</pre>
			<pre>	},</pre>
			<pre>	{</pre>
			<pre>		id: 9001,</pre>
			<pre>		name: "Nightlife",</pre>
			<pre>		type: "Category",</pre>
			<pre>		image: "images/Clubs and bars (1) - Newspaper (new version).png",</pre>
			<pre>		description: "..."</pre>
			<pre>	},</pre>
			<pre>	...</pre>
			<pre>]</pre>
		</section>
		
		<hr>
		
		<section id="welcome-message-endpoint">
			<h2>"Welcome message" endpoint</h2>
			<p>The <strong>request</strong> is formatted as:</p>
			<pre>GET {api}/welcome</pre>
			
			<p>The <strong>response</strong> is an object containing the current <a href="Data structures.html#daily-welcome-messages">Daily welcome message</a>, though only the <span class="pre">message</span> attribute is required. For example:</p>
				<pre>{</pre>
				<pre>	message: "..."</pre>
				<pre>}</pre>
		</section>
		
		<hr>
		
		<section id="things-to-do-endpoint">
			<h2>"Things to do" endpoint</h2>
			<p>This <strong>request</strong> queries for a specific date and receives a list of all Activities with an Occurrence on that date. The date is in <span class="pre">YYYY-MM-DD</span> format. The entire request is formatted as:</p>
				<pre>GET {api}/thingsToDo?date={date}</pre>
				
				<p>The <strong>response</strong> is an array of <a href="Data structures.html#activities">Activities</a> occurring on the specified day, with a <span data-updated="2014-12-04"><span class="pre">start</span> and <span class="pre">end</span> attribute</span> set to the start and end datetime of the Occurrence in the <span class="pre">yyyy-MM-dd HH:mm</span> format (for example: <span class="literal">"2014-12-31 23:59"</span>). The only other required attributes are the <span class="pre">id</span>, <span class="pre">name</span>, <span class="updated" data-datetime="2014-12-31">and <span class="pre">image</span></span> of the Activity. For example:</p>
				<pre>[</pre>
				<pre>	{</pre>
				<pre>		id: 34,</pre>
				<pre>		name: "Breakfast Buffet at The Rotunda",</pre>
				<pre>		<span data-updated="2014-12-31">image</span>: "images/Breakfast buffet.webp",</pre>
				<pre>		<span data-updated="2014-12-04">start</span>: "2014-12-04 07:00",</pre>
				<pre>		<span data-updated="2014-12-04">end</span>: "2014-12-04 12:00"</pre>
				<pre>	},</pre>
				<pre>	{</pre>
				<pre>		id: 18,</pre>
				<pre>		name: "Geocaching",</pre>
				<pre>		<span data-updated="2014-12-31">image</span>: "images/Molly finding a geocache.png",</pre>
				<pre>		<span data-updated="2014-12-04">start</span>: "2014-12-04 07:30",</pre>
				<pre>		<span data-updated="2014-12-04">end</span>: "2014-12-04 14:00"</pre>
				<pre>	},</pre>
				<pre>	{</pre>
				<pre>		id: 19,</pre>
				<pre>		name: "Dinner cruise",</pre>
				<pre>		<span data-updated="2014-12-31">image</span>: "images/Yacht dinner cruise.jpg",</pre>
				<pre>		<span data-updated="2014-12-04">start</span>: "2014-12-04 16:00",</pre>
				<pre>		<span data-updated="2014-12-04">end</span>: "2014-12-04 19:15"</pre>
				<pre>	},</pre>
				<pre>	...</pre>
				<pre>]</pre>
		</section>
		
		<hr>
		
		<section id="category-endpoint">
			<h2>"Category" endpoint</h2>
			<p>This <strong>request</strong> gets the details of a <a href="Data structures.html#categories">Category</a>, and is formatted as:</p>
			<pre>GET {api}/category?id={id}</pre>
			
			<p>The <strong>response</strong> is an object containing the details of the requested Category. For example:</p>
			<pre>{</pre>
			<pre>	name: "Golfing",</pre>
			<pre>	image: "images/golfing.jpg",</pre>
			<pre>	description: "...",</pre>
			<pre>	<span data-updated="2014-12-31">children</span>: [</pre>
			<pre>		{</pre>
			<pre>			id: 404,</pre>
			<pre>			type: "Place",</pre>
			<pre>			name: "Oakfront course",</pre>
			<pre>			<span data-updated="2014-12-31">image: "images/Places - Oakfront.jpg",</span></pre>
			<pre>			description: "..."</pre>
			<pre>		},</pre>
			<pre>		...</pre>
			<pre>	]</pre>
			<pre>}</pre>
		</section>
		
		<hr>
		
		<section id="activity-endpoint">
			<h2>"Activity" endpoint</h2>
			<p>This <strong>request</strong> gets the details of an <a href="Data structures.html#activities">Activity</a>, formatted as:</p>
			<pre>GET {api}/activity?id={id}</pre>
			
			<p>The <strong>response</strong> is an object nearly matching the <a href="Data structures.html#activities">Activity</a> model. However, instead of Series references, objects pulled from the API will include an array of the next 10 Occurrences that are upcoming, in the <span class="pre">yyyy-MM-dd HH:mm</span> format (for example: <span class="literal">"2014-12-31 23:59"</span>). For example:</p>
			<pre>{</pre>
			<pre>	id: 3,</pre>
			<pre>	name: "Back 9 on the Lakeside course",</pre>
			<pre>	description: "...",</pre>
			<pre>	image: "images/Lakeside back 9.png",</pre>
			<pre>	placeId: 4,</pre>
			<pre>	phoneNumber: "+1-877-577-5585",</pre>
			<pre>	foursquareUrl: "https://www.foursquare.com/...",</pre>
			<pre>	facebookUrl: "https://facebook.com/...",</pre>
			<pre>	twitterUrl: "https://twitter.com/...",</pre>
			<pre>	websiteUrl: "http://greenlagoon.com/golf/lakeside",</pre>
			<pre>	occurrences: [</pre>
			<pre>		"2013-05-14 09:30",</pre>
			<pre>		"2013-05-14 14:00",</pre>
			<pre>		"2013-05-15 09:30",</pre>
			<pre>		"...",</pre>
			<pre>		"2013-05-21 09:30",</pre>
			<pre>		"2013-05-21 14:00"</pre>
			<pre>	]</pre>
			<pre>}</pre>
		</section>
		
		<hr>
		
		<section id="place-endpoint">
			<h2>"Place" endpoint</h2>
			<p>This <strong>request</strong> gets the details of a <a href="Data structures.html#places">Place</a>, formatted as:</p>
			<pre>GET {api}/place?id={id}</pre>
			
			<p>The <strong>response</strong> is an object matching the <a href="Data structures.html#places">Place</a> model, with the addition of an <span class="pre">activities</span> attribute to store an array of Activities associated with this Place. Their <span class="pre">id</span>, <span class="pre">name</span>, <span data-updated="2014-12-31"><span class="pre">image</span>, and <span class="pre">description</span></span> are required. For example:</p>
			<pre>{</pre>
			<pre>	id: 4,</pre>
			<pre>	name: "Lakeside course",</pre>
			<pre>	image: "images/lakeside.png",</pre>
			<pre>	description: "...",</pre>
			<pre>	onSite: "true",</pre>
			<pre>	location: "375 Apple St, Farmville, NC",</pre>
			<pre>	phone: "+1-555-724-1274",</pre>
			<pre>	email: "golf@greenlagoon.com",</pre>
			<pre>	foursquareUrl: "https://www.foursquare.com/...",</pre>
			<pre>	facebookUrl: "https://facebook.com/...",</pre>
			<pre>	twitterUrl: "https://twitter.com/...",</pre>
			<pre>	websiteUrl: "http://greenlagoon.com/golf/lakeside",</pre>
			<pre>	activities: [</pre>
			<pre>		{</pre>
			<pre>			id: 1001,</pre>
			<pre>			name: "Back 9 on the Lakeside course",</pre>
			<pre>			<span data-updated="2014-12-31">image: "images/Lakeside back 9.png",</span></pre>
			<pre>			<span data-updated="2014-12-31">description: "..."</span></pre>
			<pre>		},</pre>
			<pre>		{</pre>
			<pre>			id: 1000,</pre>
			<pre>			name: "Front 9 on the Lakeside course",</pre>
			<pre>			<span data-updated="2014-12-31">image: "images/Lakeside front 9.png",</span></pre>
			<pre>			<span data-updated="2014-12-31">description: "..."</span></pre>
			<pre>		},</pre>
			<pre>		...</pre>
			<pre>	]</pre>
			<pre>}</pre>
		</section>
		
		<hr>
		
		<section id="notifications-endpoint">
			<h2>"Notifications" endpoint</h2>
			<p>This <strong>request</strong> gets a list of all currently active notifications that do not have a target (unless the <span class="pre">appID</span> key is used). Polling for notifications will also register the device to receive notifications, if it is not already registered. The request's format is:</p>
			<pre>GET {api}/notifications</pre>
			
			<p>The request can also contain zero or more of the following, optional key/value pairs:</p>
			<ul>
				<li>The <strong><span class="pre">since</span> key</strong> (formatted as <span class="pre">since={notification id}</span>) restricts the list of Notifications to only those sent after the notification ID given as the value. This should be used often to minimize network traffic and avoid resending unnecessary information.</li>
				<li data-updated="2014-12-31">The <strong><span class="pre">appID</span> key</strong> (formatted as <span class="pre">appID={app id}</span>) allows the API to include Notifications targeted directly to the app ID given as the value. Notifications telling a guest when their Request's status changes, for example, are targeted to the app ID which created the Request. Note that this does not restrict the list to <em>only</em> targeted Notifications, using the <span class="pre">appID</span> key includes targeted <em>and</em> untargeted Notifications in the response.</li>
			</ul>
			
			<p>The <strong>response</strong> is an array of non-expired <a href="Data structures.html#notifications">Immediate Notification</a> objects. Only the <span class="pre">id</span>, <span class="pre">heading</span>, and <span class="pre">message</span> attributes are required. For example:</p>
			<pre>[</pre>
			<pre>	{</pre>
			<pre>		id: 281,</pre>
			<pre>		heading: "Volcano tour almost full!",</pre>
			<pre>		message: "The tour to Mt. Filledwithlava is almost full. We have four spots left over, now that the Johnsons have backed out. Contact &lt;a href="tel:+1-800-555-1234"&gt;Tours and Recreation&lt;/a&gt; to reserve a spot soon!"</pre>
			<pre>	},</pre>
			<pre>	{</pre>
			<pre>		id: 282,</pre>
			<pre>		heading: "Pool is closed",</pre>
			<pre>		message: "..."</pre>
			<pre>	},</pre>
			<pre>	{</pre>
			<pre>		id: 293,</pre>
			<pre data-updated="2014-12-31">		appID: "a09152ba704cc0040d495da05e0619cb",</pre>
			<pre>		heading: "Your Housekeeping request is now In progress",</pre>
			<pre>		message: "Your Housekeeping request was updated at 12:34pm"</pre>
			<pre>	}</pre>
			<pre>]</pre>
		</section>
		
		<hr>
		
		<section id="channels-endpoint">
			<h2>"Channels" endpoint</h2>
			<p>The <strong>request</strong> is formatted as:</p>
			<pre>GET {api}/channels</pre>
			
			<p>The <strong>response</strong> should simply list out all of the current <a href="Data structures.html#channels">Channels</a> in an array. For example:</p>
			<pre>[</pre>
			<pre>	{ number: 1, name: "CNN" },</pre>
			<pre>	{ number: 2, name: "Fox News" },</pre>
			<pre>	{ number: 4, name: "PBS" },</pre>
			<pre>	{ number: 5, name: "MSNBC" }</pre>
			<pre>]</pre>
		</section>
		
		<hr>
		
		<section id="request-endpoints">
			<h2>Request endpoints</h2>
			<p>Because Requests are not only listed, but also created and updated from the API, there are actually four endpoints for Requests: One to list Request Types, one to list the details of individual Requests (including their Request Actions), one to create a new Request, and one to update an existing Request.</p>
			
			<section id="request-types-endpoint">
				<h3>"Request Types" endpoint</h3>
				<p>This endpoint only accepts <span class="pre">GET</span> <strong>requests</strong>, and is formatted as:</p>
				<pre data-updated="2014-12-31">GET {api}/requestTypes</pre>
				
				<p>The <strong>response</strong> contains all existing <a href="Data structures.html#request-types">Request Type</a> objects in an array. Only the <span class="pre">id</span>, <span class="pre">name</span>, and <span class="pre">descriptionTitle</span> attributes are required to be listed. The "Other" Request Type must be listed last. For example:</p>
				<pre>[</pre>
				<pre>	{</pre>
				<pre>		id: 2,</pre>
				<pre>		name: "Maintenance",</pre>
				<pre>		descriptionTitle: "What needs fixing?"</pre>
				<pre>	},</pre>
				<pre>	{</pre>
				<pre>		id: 3,</pre>
				<pre>		name: "Housekeeping",</pre>
				<pre>		descriptionTitle: "Is ther a time you prefer?"</pre>
				<pre>	},</pre>
				<pre>	...</pre>
				<pre>	{</pre>
				<pre>		id: 1,</pre>
				<pre>		name: "Other",</pre>
				<pre>		descriptionTitle: "How can we help?"</pre>
				<pre>	}</pre>
				<pre>]</pre>
			</section>
			
			<section id="requests-endpoint">
				<h3>"Requests" endpoint</h3>
				<p data-updated="2014-12-31">This endpoint only accepts <span class="pre">GET</span> <strong>requests</strong>, and must include an <span class="pre">appID</span> parameter. It is formatted as:</p>
				<pre data-updated="2014-12-31">GET {api}/requests?appID={app id}</pre>
				
				<aside><p><b>Note:</b> The response to a request without an app ID <em>must use</em> <span class="pre">403 Forbidden</span> to indicate that the request cannot be completed as entered. The response <em>must not use</em> <span class="pre">401 Unauthorized</span>, as this indicates the request can be completed <em>as entered</em>, provided the user supplies a proper authentication, which is incorrect.</p></aside>
				
				<p>The <strong>response</strong> consists of an array of <a href="Data structures.html#request-objects">Request</a> objects. Each Request also includes an <span class="pre">actions</span> attribute which contains an array of <a href="Data structures.html#request-actions">Request Action</a> objects. For example:</p>
				<pre>[</pre>
				<pre>	{</pre>
				<pre>		id: 1001,</pre>
				<pre>		appID: "a09152ba704cc0040d495da05e0619cb",</pre>
				<pre>		type: 2,</pre>
				<pre>		room: "42",</pre>
				<pre>		name: "John Smith",</pre>
				<pre>		description: "...",</pre>
				<pre>		status: "Accepted",</pre>
				<pre>		statusMessage: "On the way",</pre>
				<pre>		actions: [</pre>
				<pre>			{ datetime: "...", description: "..." },</pre>
				<pre>			...</pre>
				<pre>		],</pre>
				<pre>		lastUpdated: "2014-12-01 12:34"</pre>
				<pre>	},</pre>
				<pre>	...</pre>
				<pre>]</pre>
			</section>
			
			<section id="create-request-endpoint">
				<h3>"Create Request" endpoint</h3>
				<p>This endpoint <em>only accepts <span class="pre">POST</span> <strong>requests</strong></em>, and is formatted as: <span class="pre">POST {api}/createRequest</span>. The body of the request must include a JSON object with the following key/value pairs:</p>
				<table>
					<tr>
						<th>Key</th>
						<th>Value</th>
					</tr>
					<tr>
						<td><span class="pre">appID</span></td>
						<td>The application's app ID, as a string</td>
					</tr>
					<tr>
						<td><span class="pre">type</span></td>
						<td>The Request Type's ID, as an integer</td>
					</tr>
					<tr>
						<td><span class="pre">room</span></td>
						<td>The guest's room, as a string</td>
					</tr>
					<tr>
						<td><span class="pre">name</span></td>
						<td>The guest's name, as a string</td>
					</tr>
					<tr>
						<td><span class="pre">description</span></td>
						<td>The guest's description, as a string</td>
					</tr>
				</table>
				
				<p data-updated="2014-12-31">The <strong>response</strong> is the ID of the new Request (assuming it succeeded). This saves the client device from making another query to the <a href="#requests-endpoint">Requests endpoint</a> just to determine where the details can be found.</p>
			</section>
			
			<section id="update-request-endpoint">
				<h3>"Update Request" endpoint</h3>
				<p>This endpoint <em>only accepts <span class="pre">POST</span> <strong>requests</strong></em>, and is formatted as: <span class="pre">POST {api}/updateRequest</span>. The body of the request must include a JSON object, but only the identifiers and fields to be changed are required:</p>
				<table>
					<tr>
						<th>Key</th>
						<th>Value</th>
					</tr>
					<tr>
						<td><span class="pre">ID</span> (Required)</td>
						<td>The ID of the Request to be updated, as an integer</td>
					</tr>
					<tr>
						<td><span class="pre">appID</span> (Required)</td>
						<td>The application's app ID, as a string</td>
					</tr>
					<tr>
						<td><span class="pre">room</span> (Optional)</td>
						<td>The guest's updated room, as a string</td>
					</tr>
					<tr>
						<td><span class="pre">name</span> (Optional)</td>
						<td>The guest's updated name, as a string</td>
					</tr>
					<tr>
						<td><span class="pre">description</span> (Optional)</td>
						<td>The guest's updated description, as a string</td>
					</tr>
					<tr>
						<td><span class="pre">status</span> (Optional)</td>
						<td>The updated status, as a string</td>
					</tr>
				</table>
				
				<p>The <strong>response</strong> is simply the appropriate HTTP status code.</p>
			</section>
		</section>
	</body>
</html>