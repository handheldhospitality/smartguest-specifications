<!DOCTYPE html>
<html>
	<head>
		<title>Card-based Sample Style Guide</title>
		<link rel="stylesheet" href="specifications.css">
	</head>
	
	<body>
		<main>
			<header>Card-based Sample Style Guide</header>
			
			<section>
				<aside>
					<p><strong>This specification <em>is not</em> for the complete, fully-functional application, it is for the navigation-only "sample" app.</strong></p>
				</aside>
				
				<p><strong>Consistency and balance</strong> are key in a mobile app. This style guide provides some strict rules and some loose guidelines for the look and feel of this generation's SmartGuest app. Whenever you are putting together a new feature or implementing some functionality, look hard at this style guide before deciding on a layout. If you can't figure out how to use the existing building blocks, try rethinking your approach before adding complexity.</p>
				
				<aside>
					<p><b>Note:</b> This specification uses the density-independent pixel (<span class="pre">dp</span>) unit, which measures distance based on a display density of 96 pixels per inch. Devices with a significantly higher density will require measurements to be scaled.</p>
				</aside>
			</section>
			
			<section id="pages">
				<h1>Pages</h1>
				<p>The app is separated into "pages" which are the root-level containers of the content to be viewed. Pages take up the full width of the device screen, and are at least as tall as the device screen, but can be (and almost always are) longer to fit the content. Pages cannot scroll horizontally. Pages that are longer than the device screen can be scrolled vertically.</p>
				
				<p>The background of a <em>page</em> is the <span class="literal">App_Background.png</span> image, which can be found in the <span class="pre">img/images</span> directory. This image should not scroll with the rest of the page contents, it remains fixed in place while the contents move over it.</p>
			</section>
			
			<section id="animation">
				<h1>Animation</h1>
				<p><strong>Page transitions</strong> are meant to look like a stack of paper. When following a link to a <strong>deeper page</strong>, the new page slides in from the right edge of the screen to cover the old page. When following the <strong>"back" link</strong>, the old page slides off the right edge of the screen to reveal the new page underneath.</p>
				
				<p>The sidebar can be thought of like the surface the rest of the pages sit on. So when the sidebar is shown, the pages on top are slid to the right to reveal the sidebar. "Closing" the sidebar slides the pages back to their original location.</p>
			</section>
			
			<section id="colors">
				<h1>Colors</h1>
				<p>Following <a href="http://www.google.com/design/spec/material-design/introduction.html">Google's Material Design guidelines</a>, we use the client's colors boldly, but without monotonous overuse. There are <strong>four colors</strong> used in the app:</p>
				
				<table>
					<tr>
						<th>Color</th>
						<th>Hex value</th>
						<th>RGB value</th>
					</tr>
					<tr>
						<td><span class="pre">Primary-Medium</span></td>
						<td><span class="pre">#3f51b5</span></td>
						<td><span class="pre">rgb(63, 81, 181)</span></td>
					</tr>
					<tr>
						<td><span class="pre">Primary-Light</span></td>
						<td><span class="pre">#9fa8da</span></td>
						<td><span class="pre">rgb(159, 168, 218)</span></td>
					</tr>
					<tr>
						<td><span class="pre">Primary-Dark</span></td>
						<td><span class="pre">#303f9f</span></td>
						<td><span class="pre">rgb(48, 63, 159)</span></td>
					</tr>
					<tr>
						<td><span class="pre">Accent</span></td>
						<td><span class="pre">#ffc107</span></td>
						<td><span class="pre">rgb(255, 193, 7)</span></td>
					</tr>
				</table>
				
				<p>These are used in several places throughout the app to give it bold color and sharp accents, but the majority of the color scheme is still based on shades of black and white. Page definitions will give the specific colors to use for each element, if they differ from the norm, but some objects with defined colors are:</p>
				<ul>
					<li><strong>Headings</strong> and <strong>body text</strong> are black (<span class="pre">#000000</span>).</li>
					<li><strong>Headings displayed on an image</strong> are white (<span class="pre">#ffffff</span>).</li>
					<li>General UI <strong>icons</strong> are black (<span class="pre">#000000</span>).</li>
					<li><strong>Container</strong> components' background is white with 98% brightness (<span class="pre">#fafafa</span>).</li>
				</ul>
				
				<section id="opacity">
					<h2>Opacity</h2>
					<p>In addition to the base color, objects also have an opacity that helps them fit into the SmartGuest style. Fonts, icons, and similar UI elements use the following opacities:</p>
					<table>
						<tr>
							<td>
								<strong>Primary text</strong><br>
								<span class="small">Headings, sub-headings, menus, text without headings</span>
							</td>
							<td>87%</td>
						</tr>
						<tr>
							<td>
								<strong>Secondary text, icons</strong><br>
								<span class="small">Text under headings, action icons</span>
							</td>
							<td>54%</td>
						</tr>
						<tr>
							<td>
								<strong>Meta text</strong><br>
								<span class="small">Hint text, captions, inactive action icons</span>
							</td>
							<td>26%</td>
						</tr>
						<tr>
							<td>
								<strong>UI elements</strong><br>
								<span class="small">Horizontal rules, borders, edges</span>
							</td>
							<td>12%</td>
						</tr>
					</table>
					
					<p><strong>Body text</strong> normally uses the primary text opacity. If the object containing the body text uses any headings, all body text within the object uses the secondary text opacity.</p>
				</section>
			</section>
			
			<section id="typography">
				<h1>Typography</h1>
				<p>All fonts throughout the app must use the <strong>system-provided font</strong>. Do not declare a font unless there is a specific layout reason for it (like using a monospaced or an icon font).</p>
				
				<p>Fonts within the app use the following scale of sizes:</p>
				<figure>
					<img src="img/Typographic scale.png">
				</figure>
				<table>
					<tr>
						<th></th>
						<th>Font size</th>
						<th>Line height</th>
					</tr>
					<tr>
						<td>Heading text</td>
						<td><span class="pre">24dp</span>, <span class="pre">34dp</span></td>
						<td><span class="pre">32dp</span>, <span class="pre">44dp</span></td>
					</tr>
					<tr>
						<td>Menu text</td>
						<td><span class="pre">20dp</span></td>
						<td><span class="pre">26dp</span></td>
					</tr>
					<tr>
						<td>Sub-heading text</td>
						<td><span class="pre">16dp</span></td>
						<td><span class="pre">20dp</span></td>
					</tr>
					<tr>
						<td>Body text</td>
						<td><span class="pre">14dp</span></td>
						<td><span class="pre">18dp</span></td>
					</tr>
					<tr>
						<td>Small text</td>
						<td><span class="pre">12dp</span></td>
						<td><span class="pre">16dp</span></td>
					</tr>
				</table>
				
				<p><strong>Headings can be two sizes</strong>, depending on how much content they have. Headings that can fit on one line at a font size of <span class="pre">34dp</span> will use it, but anything that cannot fit on one line at <span class="pre">34dp</span> uses the <span class="pre">24dp</span> font size. For example, a short header like "Fnord" could use the <span class="pre">34dp</span> font size and not even fill the available space, while a long header like "Kangaroo Trapeze Extravaganza" would have to use the <span class="pre">24dp</span> font size and probably still overflow on to a second line.</p>
				
				<p>The <strong>action bar heading</strong> does not resize. It is always shown at a <span class="pre">24dp</span> font size.</p>
				
				<section id="scrims">
					<h2>Scrims</h2>
					<p>Heading elements with an image in the background use a text protection "scrim"; a dark shading to ensure the white heading text can be read. The scrim is black (<span class="pre">#000000</span>) and goes from 50% opacity at the outer edge to 0% opacity at the inner edge. It covers the full height of the heading text element.</p>
				</section>
				
				<section id="weight-and-decoration">
					<h2>Weight and decoration</h2>
					<p>Almost no variances in weight and decorations are applied, to keep the overall look consistent. <strong>Hyperlinks</strong> are not underlined. Strong and emphasized text is not allowed.</p>
					
					<p>The only element permitted to use decoration is small text, which is printed in italics. Small text is typically used to describe why a particular contextual card is being displayed, or provides only minor details, so using italics helps lower its visual prominence even more than its reduced size.</p>
				</section>
			</section>
			
			<section id="icons">
				<h1>Icons</h1>
				<figure>
					<img src="img/Icon sheet.png">
				</figure>
				
				<p>SmartGuest uses a set of icons using a similar visual style, balanced weight, and consistent details like corners and stroke widths. Use only the icons in the existing library. <em>Do not include additional icons</em>.</p>
			</section>
			
			<section id="action-bar">
				<h1>Action bar</h1>
				<figure>
					<img src="img/Action bar (sidebar).png">
					<img src="img/Action bar (back).png">
					<img src="img/Action bar (box model).png">
					<figcaption>
						<p>The navigation button starts immediately from the left edge of the action bar.</p>
					</figcaption>
				</figure>
				
				<p>Unlike the previous version of SmartGuest, this design only allows one action bar on a page, and it is always at the top of the page. Action bars contain a <strong>navigation button</strong>, a <strong>page title</strong> to identify which page the user is on, and some (optional) <strong>action buttons</strong>. To make navigation easier, the navigation button and page title are combined into one touch target.</p>
				
				<p>The action bar is <span class="pre">48dp</span> tall, with <span class="pre">8dp</span> of padding on the top and bottom, leaving <span class="pre">32dp</span> for content. All elements are vertically aligned to the middle of the available area.</p>
				
				<p>The <strong>navigation button</strong> starts immediately from the left edge and takes up a <span class="pre">32dp</span> square, with an <span class="pre">8dp</span> gutter on the right.</p>
				
				<p>Any <strong>action buttons</strong> start from the right edge of the bar and take up a <span class="pre">32dp</span> square. They have <span class="pre">8dp</span> of padding on the left and right, but only a total of <span class="pre">8dp</span> between buttons.</p>
				
				<p>The <strong>page title</strong> fills the remaining space and uses the <a href="#typography">heading text styles</a>. Unlike cards and other elements, however, the action bar only uses the <span class="pre">24dp</span> font size. If it overflows the available area, it is truncated and followed by an ellipsis (<span class="pre">...</span>).</p>
				
				<p>The <strong>background</strong> of the action bar uses the <span class="pre">Primary-Medium</span> color. The text and UI elements are white and use the <a href="#opacity">primary text opacity</a>.</p>
			</section>
			
			<section id="action-bar-immersive">
				<h1>Action bar (immersive pages)</h1>
				<figure>
					<img src="img/Action bar (immersive).png">
					<img src="img/Immersive action bar background.gif">
					<figcaption>
						<p>Immersive action bars allow the image to show through at the top of the page.</p>
					</figcaption>
				</figure>
				
				<p>Action bars on "immersive" pages have the same contents and spacing as action bars on normal pages, but immersive pages extend their header images up into the space normally reserved for the action bar, so a different background is required. The action bar on an immersize page has <strong>two backgrounds</strong>: The <span class="pre">Primary-Medium</span> background starts out completely transparent, and allows a <strong>scrim</strong> to show through, extending down from the top edge.</p>
				
				<p>The solid background has an opacity based on the scroll position relative to the header image. When the user's scroll position is at the top of the header image (when the page is first loaded, for example) the background is completely transparent. As the user scrolls down to the point at which the bottom of the action bar meets the bottom of the image (or lower), the background turns opaque. In other words, the opacity of the background can be defined by the function <span class="pre">Opacity = ScrollPosition / (HeaderImageHeight - ActionBarHeight)</span>.</p>
				
				<p>All <strong>text and UI elements</strong> are white on an immersive action bar.</p>
			</section>
			
			<section id="menus">
				<h1>Menus</h1>
				<figure>
					<img src="img/Menu.png">
					<img src="img/Menu (box model).png">
					<figcaption>
						<p>The menu rows are designed to look consistent even if each row uses different options.</p>
					</figcaption>
				</figure>
				
				<p>A menu is a vertical list of rows, each row having an (optional) icon for the <strong>primary identifier</strong> or action, a <strong>content area</strong>, and an (optional) icon for the <strong>secondary action</strong> or state.</p>
				
				<p>Each row is <span class="pre">56dp</span> tall. They have <span class="pre">8dp</span> of padding on all sides for a total of <span class="pre">16dp</span> between rows, and the menu itself has <span class="pre">8dp</span> of padding on the top and bottom to maintain this spacing. There is <strong>no divider</strong> between rows.</p>
				
				<p>The area for the <strong>left and right icons</strong> are <span class="pre">40dp</span> squares. Both icons have an <span class="pre">8dp</span> margin between themselves and the content area.</p>
				
				<p>The <strong>content area</strong> uses <a href="#typography">menu text styles</a>, <a href="#opacity">primary opacity</a>, and are vertically aligned to the middle. Rarely, a menu may require two lines of text to give a longer description of the row. The second row uses <a href="#typography">meta text styles</a> and <a href="#opacity">secondary opacity</a>. Both lines are vertically aligned to the middle of the content area.</p>
				
				<p>The menu typically <strong>fills the available container width</strong>, within a range of <span class="pre">260dp</span>-<span class="pre">300dp</span>.</p>
				
				<h2>Menu headers</h2>
				<p>Certain menu rows can be "headers" for the rows underneath them. These headers use exactly the same definitions as regular menu rows, except for the following:</p>
				<ul>
					<li>Their <strong>background</strong> is <span class="pre">#303f9f</span>, <span class="pre">rgb(48, 63, 159)</span>.</li>
					<li>The <strong>content area</strong> can contain one menu text line and one meta text line, as normal, <em>or</em> two body text lines using <a href="#opacity">primary opacity</a>.</li>
					<li>All text and icons in the content area are white. Text uses <a href="#opacity">primary opacity</a> and icons use <a href="#opacity">secondary opacity</a>.</li>
				</ul>
			</section>
			
			<section id="cards">
				<h1>Cards</h1>
				<figure>
					<img src="img/Cards.png">
					<img src="img/Cards (box model).png">
					<figcaption>
						<p>Cards have lots of optional elements, which keeps them visually interesting.</p>
					</figcaption>
				</figure>
				
				<p>Cards are the fundamental container of content on a page. They use images and structure of text to create a strong hierarchy that promotes scannability. Cards do not have a fixed length, they can take up whatever best fits the content, but when there are multiple cards on a page they should not cover entire length of the screen (about <span class="pre">640dp</span>-<span class="pre">720dp</span> on small devices).</p>
				
				<p>Cards can (usually) be dismissed by swiping them left or right when the user no longer needs them.</p>
				
				<p>Cards have a <strong>primary action</strong> triggered by touching anywhere in the content area. Generally this opens an expanded view of the details on the card. They can also have multiple <strong>secondary actions</strong> listed at the bottom of the card, which are described in more detail below.</p>
				
				<p>A card has a margin of <span class="pre">8dp</span> on all sides. The gutter between cards is <span class="pre">8dp</span> wide. All content within the card is separated from the edge by <span class="pre">8dp</span> and has an <span class="pre">8dp</span> gutter between elements.</p>
				
				<p>Not all cards have headings, but those that do can have one of three types of heading:</p>
				<ul>
					<li>
						<p>If the card <strong>has a heading image</strong>, its top is aligned with the top of the card and stretches the full width of the card (without padding). Aligned with the bottom edge of the heading image is a scrim and the heading text.</p>
					</li>
					<li>
						<p>If the card <strong>has a feature image</strong>, it is displayed as a <span class="pre">96dp</span> square, starting from the upper-right corner of the card (without padding). The heading text is displayed in the remaining space at the top of the card.</p>
					</li>
					<li>
						<p>If the card <strong>does not have a heading or feature image</strong>, the heading text is simply displayed at the top of the card.</p>
					</li>
				</ul>
				
				<p>In addition to the heading, cards can have any number of the following <strong>elements in this order</strong>:</p>
				<ul>
					<li>
						<p>A <strong>qualifier line</strong>, which contains starting and ending times, dates, location, or some other detail about the card which is not a multi-line description. This element uses the body text styles, and is limited to one line. Overflow is truncated and replaced with an ellipsis (<span class="pre">...</span>).</p>
					</li>
					<li>
						<p>A <strong>body section</strong>, used for the main piece of descriptive content. This element uses the body text styles and is limited to 10 lines of content. This may be in multiple paragraphs. Any overflow is truncated and replaced with an ellipsis (<span class="pre">...</span>).</p>
					</li>
					<li>
						<p>A <strong>metadata line</strong>, which is used for app-related information like explaining why a certain event was shown, or showing extra conditions when the qualifier line is already taken. The metadata line uses the meta text styles and is limited to one line. Overflow is truncated and replaced with an ellipsis (<span class="pre">...</span>).</p>
					</li>
					<li>
						<p>A list of <strong>secondary actions</strong> containing an icon and one line of text. Before each secondary action, including the first one, is a <span class="pre">1dp</span> thick divider with <span class="pre">16dp</span> of padding on the left and right. Each action item uses the following styles:</li>
						<ul>
							<li>The action is <span class="pre">42dp</span> high, with <span class="pre">8dp</span> of padding on all sides, for a <span class="pre">26dp</span> content area.</li>
							<li>The icon starts on the right edge of the content area, and takes up a <span class="pre">26dp</span> square.</li>
							<li>There is an <span class="pre">8dp</span> gutter between the icon and the text.</li>
							<li>The text uses <a href="#typography">menu text styles</a>, and is right-aligned.</li>
							<li>The text uses the <span class="pre">#3f51b5</span>, <span class="pre">rgb(63, 81, 181)</span> <strong>color</strong>.</li>
							<li>The icon uses the <span class="pre">button</span> <strong>variant</strong>. For example, the <span class="literal">Calendar button</span> icon instead of the <span class="literal">Calendar</span> icon.</li>
							<li>The icon uses the <span class="pre">indigo</span>-colored version.</li>
						</ul>
					</li>
				</ul>
				
				<p>All of the card's elements are stacked underneath each other with an <span class="pre"><span class="pre">8dp</span></span> gutter.</p>
			</section>
			
			<section id="lists">
				<h1>Lists</h1>
				<figure>
					<img src="img/List (options).png">
					<img src="img/List item (big, box model).png">
					<img src="img/List item (small, box model).png">
					<figcaption>
						<p>Lists are a great way to show lots of similar items in a way that's easy to digest.</p>
					</figcaption>
				</figure>
				
				<p>A list is a set of objects that are semantically similar to each other and have only one action per item. They combine the uniform appearance of menus with the larger content areas of cards. List items inside a card can typically be swiped away to be dismissed, though this will depend on the particular implementation of the list.</p>
				
				<p>List items come in two sizes: <strong>Big list items</strong> are <span class="pre">96dp</span> tall, <strong>small list items</strong> are <span class="pre">48dp</span> tall. Usually a list will use only one size throughout, though it is possible to mix both sizes in one list.</p>
				
				<p>If a list contains more than one list item (which they usually do), there is an <span class="pre">8dp</span> gutter between items with a <span class="pre">1dp</span> high <strong>divider</strong> in the middle. The divider has a <span class="pre">16dp</span> margin on the left and right.</p>
				
				<p>If the list item <strong>has an image</strong>, the image is shown as either a <span class="pre">96dp</span> square (on big list items) or <span class="pre">96dp</span> by <span class="pre">54dp</span> rectangle (on short list items) along the right edge of the list item. The remaining space is taken up by the <strong>content area</strong>, which has <span class="pre">8dp</span> of padding on all sides.</p>
				
				<p>The content area will always contain a <strong>heading</strong>. If the list is inside a card with its own heading, the list items' heading uses <a href="#typography">sub-heading styles</a> and can have one or two lines. If the list is not inside a card with its own heading, the list items' heading uses <a href="#typography">full heading styles</a> and can have only one line. In either case, overflow is truncated and replaced with an ellipsis (<span class="pre">...</span>). Headings are vertically aligned to the top edge of the content area.</p>
				
				<section>
					<figure>
						<img src="img/List item (big, full-head, body).png">
						<img src="img/List item (big, full-head, body, meta).png">
						<img src="img/List item (big, full-head, qual, body).png">
						
						<img src="img/List item (big, sub-head1, body).png">
						<img src="img/List item (big, sub-head1, body, meta).png">
						<img src="img/List item (big, sub-head1, qual, body).png">
						
						<img src="img/List item (big, sub-head2, body).png">
						<img src="img/List item (big, sub-head2, body, meta).png">
						<img src="img/List item (big, sub-head2, qual, body).png">
						
						<img src="img/List item (small, full-head).png">
						<img src="img/List item (small, sub-head, body).png">
						<img src="img/List item (small, sub-head, qual).png">
						<img src="img/List item (small, sub-head, meta).png">
						<figcaption>The content area of a list item has many different combinations.</figcaption>
					</figure>
					
					<p>Depending on the heading, list items can have zero, one, or two of the following <strong>elements in this order</strong>:</p>
					<ul>
						<li>
							<p>A <strong>qualifier line</strong>, which contains starting and ending times, dates, location, or some other detail about the list item which is not a multi-line description. This element uses the <a href="#typography">body text styles</a>, and is limited to one line. Overflow is truncated and replaced with an ellipsis (<span class="pre">...</span>).</p>
						</li>
						<li>
							<p>A <strong>body section</strong>, used for multi-line descriptions or the initial snippet of a longer piece. This element uses the <a href="#typography">body text styles</a> and can have up to three lines of text, depending on the other elements. See the tables below for more details.</p>
						</li>
						<li>
							<p>A <strong>metadata line</strong>, which is used for app-related information like explaining why a certain event was shown, or showing extra conditions when the qualifier line is already taken. The metadata line uses the <a href="#typography">meta text styles</a> and is limited to one line. Overflow is truncated and replaced with an ellipsis (<span class="pre">...</span>).</p>
						</li>
					</ul>
					
					<p>All of the list item's elements are stacked underneath each other with no gutter. Their line height keeps them separated. Because of the fixed height of list items, there are only certain permutations of elements allowed:</p>
					<table class="v-ruled">
						<caption>Big list items</caption>
						<tr>
							<th>Element</th>
							<th colspan="9">Maximum lines</th>
						</tr>
						<tr>
							<td>Full heading</td>
							<td><b>1</b></td>
							<td><b>1</b></td>
							<td><b>1</b></td>
							<td>0</td>
							<td>0</td>
							<td>0</td>
							<td>0</td>
							<td>0</td>
							<td>0</td>
						</tr>
						<tr>
							<td>Sub-heading</td>
							<td>0</td>
							<td>0</td>
							<td>0</td>
							<td><b>1</b></td>
							<td><b>1</b></td>
							<td><b>1</b></td>
							<td><b>2</b></td>
							<td><b>2</b></td>
							<td><b>2</b></td>
						</tr>
						<tr>
							<td>Qualifier</td>
							<td><b>1</b></td>
							<td>0</td>
							<td>0</td>
							<td><b>1</b></td>
							<td>0</td>
							<td>0</td>
							<td><b>1</b></td>
							<td>0</td>
							<td>0</td>
						</tr>
						<tr>
							<td>Body section</td>
							<td><b>1</b></td>
							<td><b>2</b></td>
							<td><b>1</b></td>
							<td><b>2</b></td>
							<td><b>3</b></td>
							<td><b>2</b></td>
							<td><b>1</b></td>
							<td><b>2</b></td>
							<td><b>1</b></td>
						</tr>
						<tr>
							<td>Metadata</td>
							<td>0</td>
							<td>0</td>
							<td><b>1</b></td>
							<td>0</td>
							<td>0</td>
							<td><b>1</b></td>
							<td>0</td>
							<td>0</td>
							<td><b>1</b></td>
						</tr>
					</table>
					
					<table class="v-ruled">
						<caption>Small list items</caption>
						<tr>
							<th>Element</th>
							<th colspan="4">Maximum lines</th>
						</tr>
						<tr>
							<td>Full heading</td>
							<td><b>1</b></td>
							<td>0</td>
							<td>0</td>
							<td>0</td>
						</tr>
						<tr>
							<td>Sub-heading</td>
							<td>0</td>
							<td><b>1</b></td>
							<td><b>1</b></td>
							<td><b>1</b></td>
						</tr>
						<tr>
							<td>Qualifier</td>
							<td>0</td>
							<td><b>1</b></td>
							<td>0</td>
							<td>0</td>
						</tr>
						<tr>
							<td>Body section</td>
							<td>0</td>
							<td>0</td>
							<td><b>1</b></td>
							<td>0</td>
						</tr>
						<tr>
							<td>Metadata</td>
							<td>0</td>
							<td>0</td>
							<td>0</td>
							<td><b>1</b></td>
						</tr>
					</table>
				</section>
			</section>
			
			<section id="forms">
				<h1>Forms</h1>
				<figure>
					<img src="img/Form (light).png">
				</figure>
				
				<p>Taking textual input on mobile devices is a highly disruptive action with numerous points of failure that can cause a user's experience to suffer. Because of this, forms should be kept to an absolute minimum and <em>alternatives should be preferred</em> in all cases. When they are unavoidable, follow these styles to keep them clear and usable.</p>
				
				<p><strong>Labels</strong> use <a href="#typography">sub-heading text styles</a> and <a href="#opacity">primary opacity</a>. They are printed directly above their input to allow for the greatest width and keep a straight line reading pattern.</p>
				
				<p><strong>Inputs</strong> use <a href="#typography">body text styles</a> and <a href="#opacity">primary opacity</a>, and have:</p>
				<ul>
					<li>An <span class="pre">8dp</span> left <strong>margin</strong> to inset them from the edge.</li>
					<li>A <span class="pre">1dp</span> thick, black <strong>border</strong>, using the <a href="#opacity">UI element opacity</a> (12%).</li>
					<li>A white <strong>background</strong> (<span class="pre">#ffffff</span>).</li>
					<li><span class="pre">2dp</span> of <strong>padding</strong> around the inside.</li>
				</ul>
				
				<p><strong>Disabled inputs</strong> use a background of 90% white (<span class="pre">#e6e6e6</span>) and <a href="#opacity">meta opacity</a> for text.</p>
				
				<p><strong>Buttons</strong> use <a href="#typography">body text styles</a> and <a href="#opacity">primary opacity</a> with white text. They also have:</p>
				<ul>
					<li><span class="pre">16dp</span> of <strong>horizontal padding</strong>.</li>
					<li><span class="pre">4dp</span> of <strong>vertical padding</strong>.</li>
					<li>A <strong>background</strong> using the <span class="pre">Accent</span> color.</li>
				</ul>
				
				<p><strong>Disabled buttons</strong> use <span class="pre">#838383</span> as the background, which is a desaturated a version of the <span class="pre">Accent</span> color.</p>
			</section>
			
			<section id="tables">
				<h1>Tables</h1>
				<figure>
					<img src="img/Table (as only content).png">
					<img src="img/Table (as part of content).png">
					<figcaption>
						<p>Tables can be the only content or part of the body content. Either way, they distinguish themselves from their surrounding elements.</p>
					</figcaption>
				</figure>
				
				<p>Some cards, like those containing the schedule for an Activity or Place, use a table to display the times which are available.</p>
				
				<p><strong>Table rows</strong> have an <span class="pre">8dp</span> left and right margin. They are each separated by an <span class="pre">8dp</span> gutter with a <span class="pre">1dp</span> high <strong>divider</strong> in the middle. The divider has a <span class="pre">16dp</span> margin on the left and right.</p>
				
				<p><strong>Table cells</strong> are vertically aligned to the top of their row, with <span class="pre">4dp</span> padding on the top and bottom, and <span class="pre">8dp</span> padding on the left and right. The horizontal padding of the cell, combined with the margin of the row, brings the total spacing from the edge of the table to the edge of the cell's content to <span class="pre">16dp</span>. This is the same horizontal spacing as the divider, which helps separate the table slightly from its surrounding content.</p>
				
				<p>Adjacent cells in a row are separated by <span class="pre">16dp</span> because of their internal padding, and can have an additional gutter of up to <span class="pre">8dp</span> to bring the maximum total space between cell contents to <span class="pre">24dp</span>. Any additional space remaining in a row is simply given to the content area of the right-most cell.</p>
			</section>
		</main>
	</body>
</html>