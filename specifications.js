// Wrap figure images with anchor tags
document.addEventListener("DOMContentLoaded", function() {
	var imgs = document.querySelectorAll("figure img");
	for(var i = 0; i < imgs.length; i++) {
		var linkedImg = document.createElement("a");
		linkedImg.href = imgs[i].src;
		linkedImg.target = "_blank";
		linkedImg.innerHTML = imgs[i].outerHTML;
		imgs[i].parentElement.replaceChild(linkedImg, imgs[i]);
	}
});

// Only highlight recent updates
document.addEventListener("DOMContentLoaded", function() {
	var now = Date.now();
	var updates = document.querySelectorAll("[data-updated]");
	for(var i = 0; i < updates.length; i++) {
		var timeAgo = now - Date.parse(updates[i].dataset.updated);
		var days = 24 * 60 * 60 * 1000;	// 1000millisec * 60sec * 60min * 24hr = 1 day
		if(timeAgo < 15 * days) updates[i].classList.add('updated');
	}
});